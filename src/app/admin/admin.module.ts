import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AlertsModule } from 'angular-alert-module';

import { AdminRoutingModule } from './admin-routing.module';
import { VeiculoModule } from './veiculo/veiculo.module';
import { DataTableModule } from 'angular-4-data-table/src/index';

import { AdminComponent } from './admin.component';

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    VeiculoModule,
    // CategoriaModule,
    AdminRoutingModule,
    DataTableModule,
    AlertsModule.forRoot()
  ],
  declarations: [AdminComponent]
})
export class AdminModule { }
