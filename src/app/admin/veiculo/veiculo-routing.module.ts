import { Routes } from '@angular/router';

import { ListaVeiculosComponent } from './lista-veiculos/lista-veiculos.component';
import { FormularioVeiculoComponent } from './formulario-veiculo/formulario-veiculo.component';
import { VisualizarVeiculoComponent } from './visualizar-veiculo/visualizar-veiculo.component';

/**
 * Define as rotas de categoria.
 */
export const VeiculoRotas: Routes = [
  {
    path: 'veiculo',
    component: ListaVeiculosComponent
  },
  {
    path: 'veiculo/inserir',
    component: FormularioVeiculoComponent
  },
  {
    path: 'veiculo/atualizar/:id',
    component: FormularioVeiculoComponent
  },
  {
    path: 'veiculo/visualizar/:id',
    component: VisualizarVeiculoComponent
  }
];
