import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Veiculo } from '../veiculo';
import { VeiculoService } from '../veiculo.service';
import { AlertsService } from 'angular-alert-module';

@Component({
  selector: 'app-formulario-veiculo',
  templateUrl: './formulario-veiculo.component.html',
  styleUrls: ['./formulario-veiculo.component.scss']
})
export class FormularioVeiculoComponent implements OnInit {

  public veiculo: Veiculo;

  /**
   * Construtor da classe.
   */
  constructor(
    private veiculoService: VeiculoService,
    private alert: AlertsService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.veiculo = new Veiculo();
    this.veiculo.id = 0;
  }

  ngOnInit() {

    const id = this.route.snapshot.paramMap.get('id');
    if (id != null) { this.carregarVeiculo(id); }
  
  }

  /**
   * Carrega o veiculo para alteração se existir.
   */
  public carregarVeiculo(id): void {

    this.veiculoService
      .getVeiculo(id)
      .subscribe(
      response => this.veiculo = response.content,
      error => this.alert.setMessage(error.message, 'error')
      );
  }

  /**
   * Método responsável por adicionar um novo veiculo no sistema.
   */
  public salvarVeiculo(veiculoForm: any): void {
    if (veiculoForm.valid) {
      const veiculo = veiculoForm.value;
      this.veiculoService.salvarVeiculo(veiculo, veiculo.id).subscribe(
        response => {
          this.alert.setMessage(response.messages.SUCCESS[0], 'success');
          this.redirecionaListagemVeiculos();
        },
        error => {
          this.alert.setMessage(error.message, 'error');
        }
      );
    }
  }

  /**
   * Redireciona o usuário para tela de veiculos.
   */
  public redirecionaListagemVeiculos(): void {
    this.router.navigate(['/admin/veiculo']);
  }
}
