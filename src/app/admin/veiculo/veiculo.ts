export class Veiculo {
  id: number
  modelo: string
  fabricante: string
  versao: string
  cor: string
  placa: string
  portas: number

  public Veiculo(
    id: number = 0,
    modelo: string = '',
    fabricante: string = '',
    versao: string = '',
    cor: string = '',
    placa: string = '',
    portas: number = 0.0
    
  ) {
    this.id = id;
    this.modelo = modelo;
    this.fabricante = fabricante;
    this.versao = versao;
    this.cor = cor;
    this.placa = placa;
    this.portas = portas;
  }
}
