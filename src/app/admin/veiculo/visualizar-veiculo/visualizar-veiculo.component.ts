import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { Veiculo } from '../veiculo';
import { VeiculoService } from '../veiculo.service';

@Component({
  selector: 'app-visualizar-veiculo',
  templateUrl: './visualizar-veiculo.component.html',
  styleUrls: ['./visualizar-veiculo.component.scss']
})
export class VisualizarVeiculoComponent implements OnInit {

  public id: any;
  public veiculo: Veiculo;

  /**
   * Construtor da classe.
   */
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private veiculoService: VeiculoService
  ) {
    this.id = this.route.snapshot.paramMap.get('id');
  }

  /**
   * Método executado quando o componente começar a renderização.
   */
  ngOnInit() {
    this.veiculoService.getVeiculo(this.id).subscribe(
      response => { this.veiculo = response.content; },
      error => console.log(error)
    );
  }

}
