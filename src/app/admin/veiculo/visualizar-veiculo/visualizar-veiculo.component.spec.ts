import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizarVeiculoComponent } from './visualizar-veiculo.component';

describe('VisualizarVeiculoComponent', () => {
  let component: VisualizarVeiculoComponent;
  let fixture: ComponentFixture<VisualizarVeiculoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualizarVeiculoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizarVeiculoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
