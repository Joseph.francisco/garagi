import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { ListaVeiculosComponent } from './lista-veiculos/lista-veiculos.component';
import { FormularioVeiculoComponent } from './formulario-veiculo/formulario-veiculo.component';
import { VisualizarVeiculoComponent } from './visualizar-veiculo/visualizar-veiculo.component';

import { VeiculoService } from './veiculo.service';

@NgModule({
  imports: [
    HttpModule,
    FormsModule,
    CommonModule,
    RouterModule
  ],
  declarations: [
    ListaVeiculosComponent,
    FormularioVeiculoComponent,
    VisualizarVeiculoComponent
  ],
  providers: [
    VeiculoService
  ]
})
export class VeiculoModule { }
