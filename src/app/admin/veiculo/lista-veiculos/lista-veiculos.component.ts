import { Component, OnInit } from '@angular/core';
import { AlertsService } from 'angular-alert-module';

import { Veiculo } from '../veiculo';
import { VeiculoService } from '../veiculo.service';

@Component({
  selector: 'app-lista-veiculos',
  templateUrl: './lista-veiculos.component.html',
  styleUrls: ['./lista-veiculos.component.scss']
})
export class ListaVeiculosComponent implements OnInit {
  public veiculos: Veiculo[];

  modelo: String;
  fabricante: String;
  versao: String;

  /**
   * Construtor da classe.
   */
  constructor(private veiculoService: VeiculoService, private alert: AlertsService) { }

  /**
   * Quando o componente inicializar.
   */
  ngOnInit(): void {
    this.veiculoService.getVeiculos().subscribe(response => {
      this.veiculos = response.content
    }, error => this.alert.setMessage(error.mensagem, 'error'));
  }

  getVeiculoModelo(modelo: String){
    if (modelo != null)
      this.veiculoService.getVeiculoModelo(modelo).subscribe(response => {
        this.veiculos = response.content
      }, error => this.alert.setMessage(error.mensagem, 'error'));
  }

  getVeiculoFabricante(fabricante){
    if(fabricante != null)
      this.veiculoService.getVeiculoFabricante(fabricante).subscribe(response => {
        this.veiculos = response.content
      }, error => this.alert.setMessage(error.mensagem, 'error'));
  }

  getVeiculoVersao(versao){
    if( versao != null)
      this.veiculoService.getVeiculoVersao(versao).subscribe(response => {
        this.veiculos = response.content
      }, error => this.alert.setMessage(error.mensagem, 'error'));
  }

  getVeiculos(){
    this.veiculoService.getVeiculos().subscribe(response => {
      this.veiculos = response.content
    }, error => this.alert.setMessage(error.mensagem, 'error'));
  }

  /**
   * Deleta um veiculo cadastrado no sistema.
   */
  deletarVeiculo(id: number, index: number): void {
    if (id != null) {
      this.veiculoService.excluir(id).subscribe(response => {
        console.log(response)
        this.alert.setMessage(response.messages.SUCCESS[0], 'success');
        this.veiculos.splice(index, 1);
      }, error => {
        this.alert.setMessage(error.mensagem, 'error');
      });
    } else {
      this.alert.setMessage('Erro ao remover veiculo.', 'error');
    }
  }
}
