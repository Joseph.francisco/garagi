import { Injectable } from '@angular/core';
import { Http, HttpModule } from '@angular/http';

import { Observable } from 'rxjs/Observable';

import { ServiceAbstract } from '../../core/service-abstract.service';
import { Veiculo } from './veiculo';

@Injectable()
export class VeiculoService extends ServiceAbstract {

  /**
   * Construtor da classe.
   *
   * @param http
   */
  constructor(private http: Http) {
    super('veiculo');
  }

  /**
   * Recupera a lista de categorias cadastradas no sistema.
   *
   * @return array
   */
  public getVeiculos(): Observable<any> {
    return this.http.get(this.getUrl()).map(this.extractData).catch(this.handleError);
  }

  public getVeiculoModelo(modelo: any): Observable<any> {
    return this.http.get(this.getUrl() + '/modelo/' + modelo).map(this.extractData).catch(this.handleError);
  }

  public getVeiculoFabricante(fabricante: any): Observable<any> {
    return this.http.get(this.getUrl() + '/fabricante/' + fabricante).map(this.extractData).catch(this.handleError);
  }

  public getVeiculoVersao(versao: any): Observable<any> {
    return this.http.get(this.getUrl() + '/versao/' + versao).map(this.extractData).catch(this.handleError);
  }

  /**
   * Recupera a categoria informada.
   *
   * @return object
   */
  public getVeiculo(id: any): Observable<any> {
    return this.http.get(this.getUrl() + '/' + id).map(this.extractData).catch(this.handleError);
  }

  /**
   * Salva um novo veiculo no sistema.
   *
   * @param veiculo
   */
  public salvarVeiculo(veiculo: Veiculo, id: number): Observable<any> {
    if (id === undefined) {
      return this.http.post(this.getUrl(), veiculo).map(this.extractData).catch(this.handleError);
    } else {
      return this.http.put(this.getUrl(), veiculo).map(this.extractData).catch(this.handleError);
    }
  }

  /**
   * Exclui a categoria com o id informado.
   *
   * @param id
   * @return object
   */
  public excluir(id: number): Observable<any> {
    return this.http.delete(this.getUrl() + '/' + id).map(this.extractData).catch(this.handleError);
  }
}
