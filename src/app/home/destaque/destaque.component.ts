import { Component, OnInit } from '@angular/core';

import { Veiculo } from '../../admin/veiculo/veiculo';
import { VeiculoService } from '../../admin/veiculo/veiculo.service';

@Component({
  selector: 'app-destaque',
  templateUrl: './destaque.component.html',
  styleUrls: ['./destaque.component.scss']
})
export class DestaqueComponent implements OnInit {
  public veiculos: Veiculo[];

  /**
   * Construtor da classe.
   *
   * @param veiculoService
   */
  constructor(
    private veiculoService: VeiculoService
  ) {}

  /**
   * Quando o componente estiver carregando.
   */
  ngOnInit() {
    this.veiculoService.getVeiculos().subscribe(
      response => {
        console.log(response);
        this.veiculos = response.content;
      },
      error => {
        console.log(error);
      }
    );
  }
}
